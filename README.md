# PyAutoGUI

PyAutoGUIは、Pythonのライブラリであり、プログラムを通じてマウスやキーボードの操作を自動化するために使われます。これは特にGUI（グラフィカルユーザーインターフェース）を持つアプリケーションの自動テストや、反復的な作業を自動化するために役立ちます。以下はPyAutoGUIの主な特徴と機能についての概要です：

1. マウスの操作の自動化：

マウスの移動、クリック、ドラッグアンドドロップなどの操作を自動化できます。
絶対座標または相対座標でマウスを操作できます。

2. キーボードの操作の自動化：

文字の入力やキーストロークの送信が可能です。
キーボードのショートカットの実行もサポートされています。

3. スクリーンショットと画像認識：

スクリーンショットを撮る機能があり、これを使って画面上の特定の要素を見つけることができます。
画像認識を使用して、画面上のアイコンやボタンなどを検出し、自動操作のターゲットとすることができます。

4. ウィンドウ管理：

オープンされているウィンドウのリストを取得し、特定のウィンドウをアクティブにすることができます。
ウィンドウのサイズや位置を調整することも可能です。

5. スクリプトとの統合：

Pythonスクリプト内で容易に使用でき、他のPythonライブラリやフレームワークと組み合わせることができます。
PyAutoGUIは、多くの場合で非常に便利ですが、スクリプトが実行されている間はコンピューターの制御を失うことに注意が必要です。また、画面解像度や画面のレイアウトが変更された場合には、事前に設定した座標が正しく機能しない可能性があります。そのため、安定した環境で使用することが推奨されます。


##
```
import time
import pyautogui

# Ctl+F5
pyautogui.hotkey('ctrl', 'f5')

time.sleep(5)
pyautogui.press("space")
time.sleep(5)
pyautogui.press("space")

```


https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/120.0.6099.224/linux64/chromedriver-linux64.zip


## SSH経由で実行する

```
ssh 
export DISPLAT=:0
```

